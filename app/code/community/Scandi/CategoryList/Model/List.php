<?php

class Scandi_CategoryList_Model_List extends Mage_Core_Model_Abstract
{

    public function getCategoryList($category_id) {
        $result = array();
        $result[] = Mage::getModel('catalog/category')->load($category_id);
        $children = Mage::getModel('catalog/category')->getCategories($category_id);
        foreach ($children as $category) {
            $result[] = $category;
        }
        return $result;
    }
}