<?php

class Scandi_CategoryList_Block_List extends Mage_Core_Block_Template
{
    public function getCategoryList($category_id) {
        $arr_categories = array();
        $categories = Mage::getModel("categorylist/list")->getCategoryList($category_id);

        foreach ($categories as $category) {
            $arr_categories[] = array(
                'id'    => $category->getId(),
                'name'  => $category->getName(),
                'url'   => Mage::getModel('catalog/category')->load($category->getId())->getUrl(),
            );
        }

        return $arr_categories;
    }
}